package com.example.rushan.mynote3;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ContentValues;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.SparseBooleanArray;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    String server_name = "http://noteproject.000webhostapp.com";
    ArrayList<String> list = new ArrayList<>(); //список заметок
    ArrayAdapter<String> adapter; //отображалка

    SQLiteDatabase noteDBlocal;
    String username, message;

    INSERTtoNote insert_to_chat; // класс отправляет новое сообщение на сервер

    UpdateReceiver upd_res;
    Cursor c;

    ContentValues new_mess;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        this.startService(new Intent(this, FoneService.class));

        Toast toast = Toast.makeText(getApplicationContext(),"Вы зашли как: " + getUsername(), Toast.LENGTH_LONG);
        toast.show();

        username = getUsername();

        noteDBlocal = openOrCreateDatabase("noteDBlocal.db", Context.MODE_PRIVATE, null);
        noteDBlocal.execSQL("CREATE TABLE IF NOT EXISTS note (id integer primary key autoincrement, username, message)");

        final ListView listView = (ListView) findViewById(R.id.listView);

        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, list);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                adapter.remove(adapter.getItem(position));
            }
        });

        upd_res = new UpdateReceiver();
        registerReceiver(upd_res, new IntentFilter("com.example.rushan.action.UPDATE_ListView"));

        adapter.notifyDataSetChanged();
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

    }
    public String getUsername() {
        AccountManager manager = AccountManager.get(this);
        Account[] accounts = manager.getAccountsByType("com.google");
        List<String> possibleEmails = new LinkedList<String>();

        for (Account account : accounts) {
            // TODO: Check possibleEmail against an email regex or treat
            // account.name as an email address only for certain account.type values.
            possibleEmails.add(account.name);
        }

        if (!possibleEmails.isEmpty() && possibleEmails.get(0) != null) {
            String email = possibleEmails.get(0);
            return email;
        }
        return null;
    }

    public void onClick(View view) {
        //получить значение из поля
        TextView textView = (TextView)findViewById(R.id.editText1);
        String note = textView.getText().toString();
        message = note;
        if (textView.length() != 0)
        {
            insert_to_chat = new INSERTtoNote();
            insert_to_chat.execute();
            //добавить в список

            new_mess = new ContentValues();
            new_mess.put("username", username);
            new_mess.put("message", message);
            noteDBlocal.insert("note", null, new_mess);
            new_mess.clear();
            Log.i("note", "+ FoneService в ЛОК БД добавлено " + message);

            list.add(note);
            //отобразить в listview
            ListView listView = (ListView)findViewById(R.id.listView);
            adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, list);
            listView.setAdapter(adapter);
            textView.setText("");
            adapter.notifyDataSetChanged();
        }
        else{
            Toast toast = Toast.makeText(getApplicationContext(),"Поле оказалось пустым", Toast.LENGTH_LONG);
            toast.show();
        }
    }

    public void onClick2(View view) {
        noteDBlocal.execSQL("DELETE FROM note");
        Log.i("note", " + FoneService БД удален");
        ListView listView = (ListView)findViewById(R.id.listView);
        listView.setAdapter(null);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    // обновим lv = заполним нужные позиции в lv информацией из БД
    @SuppressLint("SimpleDateFormat")
    public void create_lv() {
        try{
            c = noteDBlocal.rawQuery("SELECT message FROM note WHERE username = '" + username + "'",null);
        }
        catch (Exception e)
        {
            Log.i("note", "+ FoneService ---------- ошибка курсора:\n" + e.getMessage());
        }
        Log.i("note", "FoneService Курсор :" + String.valueOf(c.getCount()));
        if (c != null)
        {
            if (c.moveToFirst())
            {
                while ( !c.isAfterLast() )
                {
                    //String a = c.getString(c.getColumnIndex("username"));
                    String b = c.getString(c.getColumnIndex("message"));
                    Log.i("note", "+ FoneService username " + " message " + b );
                    list.add(b);
                    ListView listView = (ListView)findViewById(R.id.listView);
                    adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, list);
                    listView.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                    c.moveToNext();
                    Log.i("note", "+ FoneService ======================== добавили поле");
                }
            }
        }
        Log.i("note", "+ FoneService ======================== обновили поле чата");
    }

    // ресивер приёмник ждет сообщения от FoneService
    // если сообщение пришло, значит есть новая запись в БД - обновим ListView
    public class UpdateReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.i("chat", "+ FoneService - ресивер получил сообщение - обновим ListView");
            create_lv();
        }
    }
    // отправим сообщение на сервер
    private class INSERTtoNote extends AsyncTask<Void, Void, Integer> {

        HttpURLConnection conn;
        Integer res;

        protected Integer doInBackground(Void... params) {

            try {

                // соберем линк для передачи новой строки
                String post_url = server_name
                        + "?action=insert&username="
                        + URLEncoder.encode(username, "UTF-8")
                        + "&message="
                        + URLEncoder.encode(message, "UTF-8");

                Log.i("note", "+ FoneService - отправляем на сервер новое сообщение: ");

                URL url = new URL(post_url);
                conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("POST");
                conn.setRequestProperty("User-Agent", "Mozilla/5.0");
                conn.connect();

                res = conn.getResponseCode();
                Log.i("note", "+ FoneService - ответ сервера (200 - все ОК): " + res.toString());

            } catch (Exception e) {
                Log.i("note", "+ FoneService - ошибка соединения: " + e.getMessage());

            } finally {
                // закроем соединение
                conn.disconnect();
            }
            return res;
        }

        protected void onPostExecute(Integer result) {

            try {
                if (result == 200) {
                    Log.i("note", "+ ChatActivity - сообщение успешно ушло.");
                }
            } catch (Exception e) {
                Log.i("note", "+ ChatActivity - ошибка передачи сообщения:\n"
                        + e.getMessage());
                Toast.makeText(getApplicationContext(),
                        "ошибка передачи сообщения", Toast.LENGTH_SHORT).show();
            } finally {
                // активируем кнопку
            }
        }
    }
}
