package com.example.rushan.mynote3;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.BitmapFactory;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by Rushan on 11.01.2017.
 */
public class FoneService extends Service {
    // ИМЯ СЕРВЕРА (url зарегистрированного нами сайта)
    // например http://l29340eb.bget.ru
    String server_name = "http://noteproject.000webhostapp.com";

    SQLiteDatabase noteDBlocal;
    HttpURLConnection conn;
    Thread thr;
    ContentValues new_mess;
    int k = 0;
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        noteDBlocal = openOrCreateDatabase("noteDBlocal.db", Context.MODE_PRIVATE, null);
        noteDBlocal.execSQL("CREATE TABLE IF NOT EXISTS note (id integer primary key autoincrement, username, message)");

        startLoop();
        return super.onStartCommand(intent, flags, startId);
    }

    public void startLoop() {
        thr = new Thread(new Runnable() {

            String answer, lnk;
            ArrayList<String> list = new ArrayList<>();

            public void run() {

                lnk = server_name + "/?action=select";
                Cursor c = noteDBlocal.rawQuery("SELECT * FROM note", null);
                if(c != null )
                {
                    String username = "";
                    String message = "";
                    Log.i("note", "+ FoneService ЛОК-БД НЕ ПУСТОЙ ");
                    if  (c.moveToFirst()) {
                        do {
                            username = c.getString(c.getColumnIndex("username"));
                            message = c.getString(c.getColumnIndex("message"));
                            Log.i("note", "+ FoneService курсор:" + username + message);
                            list.add(username + message);
                        }while (c.moveToNext());
                    }
                    //noteDBlocal.execSQL("DELETE FROM note");
                }
                else{
                    Log.i("note", "+ FoneService ЛОК-БД ПУСТОЙ ");
                }
                try {
                    conn = (HttpURLConnection) new URL(lnk).openConnection();
                    conn.setRequestMethod("POST");
                    conn.setReadTimeout(5000);
                    conn.setConnectTimeout(5000);
                    conn.setRequestProperty("User-Agent", "Mozilla/5.0");
                    conn.setDoInput(true);
                    conn.connect();
                } catch (Exception e) {
                    Log.i("note", "+ FoneService ошибка при подключ к HTTTP : " + e.getMessage());
                }
                finally{
                    c.close();
                }

                try {
                    InputStream is = conn.getInputStream();
                    BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                    StringBuilder sb = new StringBuilder();
                    String bfr_st = null;
                    while ((bfr_st = br.readLine()) != null) {
                        sb.append(bfr_st);
                    }

                    Log.i("note", "+ FoneService - полный ответ сервера:\n" + sb.toString());
                    // сформируем ответ сервера в string
                    // обрежем в полученном ответе все, что находится за "]"
                    // это необходимо, т.к. json ответ приходит с мусором
                    // и если этот мусор не убрать - будет невалидным
                    answer = sb.toString();
                    answer = answer.substring(0, answer.indexOf("]") + 1);

                    is.close(); // закроем поток
                    br.close(); // закроем буфер

                } catch (Exception e) {
                    Log.i("note", "+ FoneService ошибка: " + e.getMessage());
                } finally {
                    conn.disconnect();
                    Log.i("note", "+ FoneService ЗАКРОЕМ СОЕДИНЕНИЕ");
                }

                // запишем ответ в БД ---------------------------------->
                if (answer != null && !answer.trim().equals("")) {
                    Log.i("note", "+ FoneService ответ содержит JSON:");
                    try {
                        // ответ превратим в JSON массив
                        JSONArray ja = new JSONArray(answer);
                        JSONObject jo;

                        Integer i = 0;

                        while (i < ja.length()) {

                            // разберем JSON массив построчно
                            jo = ja.getJSONObject(i);

                            Log.i("note", "FoneService =================>>> " + jo.getString("username") + " | "
                                    + jo.getString("message"));

                            // создадим новое сообщение
                            new_mess = new ContentValues();
                            new_mess.put("username", jo.getString("username"));
                            new_mess.put("message", jo.getString("message"));
                            String s = jo.getString("username")+jo.getString("message");
                            // запишем новое сообщение в БД
                            if (list.size()>0)
                            {
                                int repeat = 0;
                                Log.i("note", "+ FoneService СПИСОК ИЗ ЛОК-БД БОЛЬШЕ НУЛЯ - "+ list.size());
                                for(int k = 0; k < list.size();k++)
                                {
                                    if(s.equals(list.get(k)))
                                    {
                                        repeat++;
                                        Log.i("note", "+ FoneService ПОВТОР " + s +" == "+ list.get(k));
                                    }
                                }
                                //Log.i("note", "+ FoneService ПОВТОРЫ" + repeat);
                                if(repeat == 0)
                                {
                                    Log.i("note", "+ FoneService НЕ ПОВТОР: " + s);
                                    noteDBlocal.insert("note", null, new_mess);
                                    Log.i("note", "+ FoneService ДОБАВЛЕНО в БД: " + jo.getString("username") +" "+ jo.getString("message"));
                                }
                            }
                            else {
                                Log.i("note", "+ FoneService ЛОК-БД БЫЛО ПУСТЫМ - "+ list.size());
                                noteDBlocal.insert("note", null, new_mess);
                                Log.i("note", "+ FoneService ДОБАВЛЕНО в БД: " + jo.getString("username") +" "+ jo.getString("message"));
                            }
                            new_mess.clear();
                            i++;
                            // отправим броадкаст для ChatActivity
                            // если она открыта - она обновить ListView
                        }
                        Log.i("note", "+ FoneService ОТПРАВЛЕН ЗАПРОС НА ОБНОВЛЕНИЕ ЛИСТА");
                        sendBroadcast(new Intent("com.example.rushan.action.UPDATE_ListView"));
                    } catch (Exception e) {
                        // если ответ сервера не содержит валидный JSON
                        Log.i("note", "+ FoneService ---------- ошибка ответа сервера:\n" + e.getMessage());
                    }
                } else {
                    // если ответ сервера пустой
                    Log.i("note", "+ FoneService ---------- ответ не содержит JSON!");
                }
                try {
                    Thread.sleep(15000);
                } catch (Exception e) {
                    Log.i("note", "+ FoneService - ошибка процесса: " + e.getMessage());
                }
            }
        });
        thr.setDaemon(true);
        thr.start();
    }
}
